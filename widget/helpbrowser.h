/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Definition of the HelpBrowser class
** File name: helpbrowser.h
**
****************************************************************/

#ifndef HELPBROWSER_H
#define HELPBROWSER_H

#include <QDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QWidget>

//! The HelpBrowser class provides a program settings widget.
/*!
        The HelpBrowser class shows three program settings. Setting the
        layout of the virtual keyboard, setting the ticker speed and font
        and doing a reset of the user tables.

        @author Tom Thielicke, s712715
        @version 0.0.3
        @date 18.06.2006
*/
class HelpBrowser : public QDialog {
    Q_OBJECT

public:
    //! Constructor, creates two table objects and provide it in two tabs.
    /*!
            In this contructor three groups are created over functions
            createGroupKeyboardLayout(), createGroupUserReset() and
            createGroupTickerFont(). In addition, standard settings are read,
            standard font is set and connections are set.

            @param parent The parent QWidget
            @see createGroupKeyboardLayout(), createGroupUserReset(),
                    createGroupTickerFont(), readSettings(),
                    tickerFont
    */
    HelpBrowser(QString link, QWidget* parent = 0);

public slots:

private slots:

    //! Start button pressed
    void clickClose();

    void clickPrint();

    void changePage(QUrl url);

private:
    //! Creates a cancel and a save button.
    void createButtons();

    //! Creates the layout of the complete class.
    void createLayout();

    QPushButton* buttonClose;
    QPushButton* buttonBack;
    QPushButton* buttonHome;
    QTextBrowser* textBrowser;
    QPushButton* buttonPrint;
};

#endif // HELPBROWSER_H
