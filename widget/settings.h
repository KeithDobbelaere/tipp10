/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions
Copyright (c) 2021, André Apitzsch

SPDX-License-Identifier: GPL-2.0-only
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QFont>

namespace Ui {
class Settings;
}

class Settings : public QDialog {
    Q_OBJECT

public:
    explicit Settings(QWidget* parent = nullptr);
    ~Settings();

private slots:
    void on_buttonFont_clicked();
    void on_buttonFontColor_clicked();
    void on_buttonBackground_clicked();
    void on_buttonCursor_clicked();
    void on_buttonLayoutAdvanced_clicked();
    void on_buttonResetLessons_clicked();
    void on_buttonResetRecordedChars_clicked();
    void checkLessonToLayout();
    void clearLayoutSetting();

private:
    void showHelp();
    void save();
    void readSettings();
    bool writeSettings();
    void setFontButtonLabel();

    Ui::Settings* ui;
    QFont tickerFont;
};

#endif // SETTINGS_H
