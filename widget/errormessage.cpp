/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Implementation of the ErrorMessage class
** File name: errormessage.cpp
**
****************************************************************/

#include <QMessageBox>

#include "def/defines.h"
#include "errormessage.h"

ErrorMessage::ErrorMessage(QWidget* parent)
    : QWidget(parent)
{
}

void ErrorMessage::showMessage(
    Error errorNo, Type errorType, Cancel cancelProcedure, QString addon)
{
    QString message;
    // First generate the error text
    message = getErrorText(errorNo);
    // If exist, append an additional text
    if (addon != "") {
        message.append("\n" + addon);
    }
    /*if (cancelProcedure != ErrorMessage::Cancel::No) {
            // Append fix text
            message.append(tr("\n\nBitte melden Sie den Fehler "
                    "(Fehlernummer) und die Umstaende unter denen er aufgetreten
    " "ist im Internet unter: ") + t10::app_url
                    + tr("\nSo kann ") + t10::app_name
                    + tr(" verbessert werden. Danke!"));
    }*/

    // Append, what the program do now
    message.append("\n\n" + getCancelText(cancelProcedure));

    // Choose a message style
    switch (errorType) {
    case Type::Info:
        QMessageBox::information(nullptr, t10::app_name, message);
        break;
    case Type::Warning:
        QMessageBox::warning(nullptr, t10::app_name, message);
        break;
    case Type::Critical:
        QMessageBox::critical(nullptr, t10::app_name, message);
        break;
    }
}

QString ErrorMessage::getCancelText(Cancel type)
{
    QString cancelText = "";
    switch (type) {
    case Cancel::No:
        cancelText = "";
        break;
    case Cancel::Operation:
        cancelText = tr("The process will be aborted.");
        break;
    case Cancel::Update:
        cancelText = tr("The update will be aborted.");
        break;
    case Cancel::Program:
        cancelText = tr("The program will be aborted.");
        break;
    }
    return cancelText;
}

QString ErrorMessage::getErrorText(Error number)
{
    QString errorText = "";
    switch (number) {
    case Error::logo_pic:
        errorText = tr("Cannot load the program logo.");
        break;
    case Error::key_pic:
        errorText = tr("Cannot load the keyboard bitmap.");
        break;
    case Error::ticker_pic:
        errorText = tr("Cannot load the timer background.");
        break;
    case Error::status_pic:
        errorText = tr("Cannot load the status bar background.");
        break;

    case Error::sql_db_app_exist:
        errorText
            = tr("Cannot find the database %1. The file could not be "
                 "imported.\nPlease check whether it is a readable text file.")
                  .arg(t10::app_db);
        break;
    case Error::sql_db_user_exist:
        errorText = tr("Cannot find the database in the specified "
                       "directory.\nTIPP10 is trying to create a new, empty "
                       "database in the specified directory.\n\nYou can change "
                       "the path to the database in the program settings.\n");
        break;
    case Error::sql_db_app_copy:
        errorText = tr(
            "Cannot create the user database in your HOME directory. There may "
            "be no permission to write.\nTIPP10 is trying to use the original "
            "database in the program directory.\n\nYou can change the path to "
            "the database in the program settings later.\n");
        break;
    case Error::sql_db_user_copy:
        errorText = tr("Cannot create the user database in the specified "
                       "directory. There may be no directory or no permission "
                       "to write.\nTIPP10 is trying to create a new, empty "
                       "database in your HOME directory.\n\nYou can change the "
                       "path to the database in the program settings later.\n");
        break;
    case Error::sql_connection:
        errorText = tr("Connection to the database failed.");
        break;

    case Error::db_version_exist:
        errorText = tr("Connection to the database failed.");
        break;
    case Error::user_lessons_flush:
        errorText = tr("The user table with lesson data  cannot be "
                       "emptied.\nSQL statement failed.");
        break;
    case Error::user_errors_flush:
        errorText = tr("The user table with error data cannot be emptied.\nSQL "
                       "statement failed.");
        break;
    case Error::lessons_exist:
        errorText = tr("No lessons exist.");
        break;
    case Error::lessons_selected:
        errorText = tr("No lesson selected.\nPlease select a lesson.");
        break;
    case Error::lessons_creation:
        errorText = tr("Cannot create the lesson.\nSQL statement failed.");
        break;
    case Error::lessons_update:
        errorText
            = tr("The lesson could not be updated because there is no\naccess "
                 "to the database.\n\nIf this problem only occurred after the "
                 "software had been\nrunning smoothly for some time, the "
                 "database is expected\nto have been damaged (eg crashing of "
                 "the computer).\nTo check whether or not the database has "
                 "been damaged,\nyou can rename the database file and restart "
                 "the software (it\nwill create a new, empty database "
                 "automatically).\nYou can find the database path \"%1\" in "
                 "the General Settings.\n\nIf this problem occurred after the "
                 "first time the software was\nstarted, please check the write "
                 "privileges on the database\nfile.")
                  .arg(t10::app_user_db);
        break;
    case Error::user_errors_refresh:
        errorText
            = tr("The user typing errors could not be updated because\nthere "
                 "is no access to the database.\n\nIf this problem only "
                 "occurred after the software had been\nrunning smoothly for "
                 "some time, the database is expected\nto have been damaged "
                 "(eg crashing of the computer).\nTo check whether or not the "
                 "database has been damaged,\nyou can rename the database file "
                 "and restart the software (it\nwill create a new, empty "
                 "database automatically).\nYou can find the database path "
                 "\"%1\" in the General Settings.\n\nIf this problem occurred "
                 "after the first time the software was\nstarted, please check "
                 "the write privileges on the database\nfile.")
                  .arg(t10::app_user_db);
        break;
    case Error::user_lessons_refresh:
        errorText
            = tr("The user lesson data could not be updated because\nthere is "
                 "no access to the database.\n\nIf this problem only occurred "
                 "after the software had been\nrunning smoothly for some time, "
                 "the database is expected\nto have been damaged (eg crashing "
                 "of the computer).\nTo check whether or not the database has "
                 "been damaged,\nyou can rename the database file and restart "
                 "the software (it\nwill create a new, empty database "
                 "automatically).\nYou can find the database path \"%1\" in "
                 "the General Settings.\n\nIf this problem occurred after the "
                 "first time the software was\nstarted, please check the write "
                 "privileges on the database\nfile.")
                  .arg(t10::app_user_db);
        break;
    case Error::user_lesson_add:
        errorText = tr("Cannot save the lesson.\nSQL statement failed.");
        break;
    case Error::user_lesson_get:
        errorText = tr("Cannot retrieve the lesson.\nSQL statement failed.");
        break;
    case Error::user_lesson_analyze:
        errorText = tr("Cannot analyze the lesson.\nSQL statement failed.");
        break;
    case Error::user_import_read:
        errorText = tr("The file could not be imported.\nPlease check whether "
                       "it is a readable text file.\n");
        break;
    case Error::user_import_empty:
        errorText
            = tr("The file could not be imported because it is empty.\nPlease "
                 "check whether it is a readable text file with content.\n");
        break;
    case Error::user_download_execution:
        errorText
            = tr("The file could not be imported.\nPlease check the spelling "
                 "of the web address;\nit must be a valid URL and a readable "
                 "text file.\nPlease also check your internet connection.");
        break;
    case Error::user_export_write:
        errorText = tr("The file could not be exported.\nPlease check to see "
                       "whether it is a writable text file.\n");
        break;
    case Error::temp_file_creation:
        errorText = tr("Cannot create temporary file.");
        break;
    case Error::update_execution:
        errorText = tr("Cannot execute the update process.\nPlease check your "
                       "internet connection and proxy settings.");
        break;
    case Error::online_version_readable:
        errorText = tr("Cannot read the online update version.");
        break;
    case Error::db_version_readable:
        errorText = tr("Cannot read the database update version.");
        break;
    case Error::update_sql_execution:
        errorText = tr("Cannot execute the SQL statement.");
        break;
    case Error::error_defines_exist:
        errorText = tr("Cannot find typing mistake definitions.");
        break;
    case Error::lesson_content_exist:
        errorText = tr("Cannot create temporary file.\nUpdate failed.");
        break;
    case Error::analyze_table_creation:
        errorText = tr("Cannot create analysis table.");
        break;
    case Error::analyze_index_creation:
        errorText = tr("Cannot create analysis index.");
        break;
    case Error::analyze_table_fill:
        errorText = tr("Cannot fill analysis table with values.");
        break;

    default:
        errorText = tr("An error has occured.");
        break;
    }
    // Append the error number
    errorText.append(tr("\n(Error number: %1)\n")
                         .arg(QString::number(static_cast<int>(number))));
    return errorText;
}
