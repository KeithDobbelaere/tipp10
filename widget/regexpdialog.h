/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions
Copyright (c) 2021, André Apitzsch

SPDX-License-Identifier: GPL-2.0-only
*/

#ifndef REGEXPDIALOG_H
#define REGEXPDIALOG_H

#include <QDialog>

namespace Ui {
class RegExpDialog;
}

class RegExpDialog : public QDialog {
    Q_OBJECT

public:
    explicit RegExpDialog(QString layout, QWidget* parent = nullptr);
    ~RegExpDialog();

private slots:
    void getDefault();

private:
    void save();
    void readSettings();
    void writeSettings();

    Ui::RegExpDialog* ui;
    QString currentLayout;
};

#endif // REGEXPDIALOG_H
