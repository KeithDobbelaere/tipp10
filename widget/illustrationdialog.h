/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Definition of the SettingDialog class
** File name: illustrationdialog.h
**
****************************************************************/

#ifndef ILLUSTRATIONDIALOG_H
#define ILLUSTRATIONDIALOG_H

#include <QCheckBox>
#include <QDialog>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QWidget>

//! The IllustrationDialog class provides a program settings widget.
/*!
    The IllustrationDialog class shows three program settings. Setting the
    layout of the virtual keyboard, setting the ticker speed and font
    and doing a reset of the user tables.

    @author Tom Thielicke, s712715
    @version 0.0.3
    @date 18.06.2006
*/
class IllustrationDialog : public QDialog {
    Q_OBJECT

public:
    //! Constructor, creates an illustration widget.
    /*!

        @param parent The parent QWidget
    */
    IllustrationDialog(QWidget* parent = 0);

public slots:

private slots:

    //! Start button pressed
    void clickStart();

private:
    //! Creates a cancel and a save button.
    void createButtons();

    //! Creates content widget
    void createContent();

    //! Creates the layout of the complete class.
    void createLayout();

    //! Writes user settings
    void writeSettings();

    QPushButton* buttonStart;
    QCheckBox* showDialogCheck;

    QTextBrowser* illustrationContent;
};

#endif // ILLUSTRATIONDIALOG_H
