/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Implementation of the LessonPrintDialog class
** File name: lessonprintdialog.cpp
**
****************************************************************/

#include <QHBoxLayout>
#include <QVBoxLayout>

#include "lessonprintdialog.h"

LessonPrintDialog::LessonPrintDialog(QString* enteredName, QWidget* parent)
    : QDialog(parent)
    , userName(enteredName)
{
    setWindowFlags(windowFlags() ^ Qt::WindowContextHelpButtonHint);

    setWindowTitle(tr("Print Lesson"));

    // Create texbox
    createLineEdit();

    // Create buttons
    createButtons();

    // Set the layout of all widgets created above
    createLayout();

    lineName->setFocus();
}

void LessonPrintDialog::createButtons()
{
    // Buttons
    buttonOk = new QPushButton(this);
    buttonCancel = new QPushButton(this);

    buttonOk->setText(tr("&Print"));
    buttonCancel->setText(tr("&Cancel"));

    buttonOk->setDefault(true);
    // Widget connections
    connect(buttonOk, &QPushButton::clicked, this, &LessonPrintDialog::clickOk);
    connect(
        buttonCancel, &QPushButton::clicked, this, &LessonPrintDialog::reject);
}

void LessonPrintDialog::createLineEdit()
{

    lineName = new QLineEdit();

    labelName = new QLabel(tr("Please enter your name:"));

    labelName->setWordWrap(true);
}

void LessonPrintDialog::createLayout()
{
    // Button layout horizontal
    QHBoxLayout* buttonLayoutHorizontal = new QHBoxLayout;
    buttonLayoutHorizontal->addStretch(1);
    buttonLayoutHorizontal->addWidget(buttonCancel);
    buttonLayoutHorizontal->addWidget(buttonOk);
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addSpacing(1);
    mainLayout->addWidget(labelName);
    mainLayout->addSpacing(1);
    mainLayout->addWidget(lineName);
    mainLayout->addSpacing(1);
    mainLayout->addLayout(buttonLayoutHorizontal);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void LessonPrintDialog::clickOk()
{

    // Return entered name
    *userName = lineName->text();
    accept();
}
